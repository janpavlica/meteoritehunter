# Meteorite Hunter

Since the beginning of time, our planet has been hit by many small and large meteorites. NASA provides information on all known landings. Now you can play meteorite hunter, moving around the planet looking for meteorite craters. A bigger meteorite means a bigger crater and the better your chances of getting a meteorite fragment.

![](intro.gif)

## Features

- Move around the map anywhere in the world.
- Search for meteorites at a selected location on the map.
- Quickly move to the current or specified location on the map.
- Results with an overview of meteorites found.
- Help for developers in DEBUG mode.

## (Clean) Architecture 

UIKit without Storyboards, MVVM+C, Combine (for presentation layer), Async/Await (for domain and data layer), CoreData

## Requirements

- iOS 15.0+
- Xcode 11.0

## Author

**Jan Pavlica**\
palivko@gmail.com / [LinkedIn](https://www.linkedin.com/in/jan-pavlica/)

Distributed under the MIT license. See ``LICENSE`` for more information.
