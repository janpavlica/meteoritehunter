//
//  MeteoritesRepositoryTests.swift
//  MeteoriteHunterTests
//
//  Created by Jan Pavlica on 06.02.2023.
//

import XCTest
import CoreData
@testable import Meteorite_Hunter

final class MeteoritesRepositoryTests: XCTestCase {

    private var modelName: String!
    private var api: MeteoriteLandingsAPI!
    private var dataStack: TestDataStack!
    private var storage: MeteoritesStorage!
    private var repository: MeteoritesRepository!

    override func setUp() {

        super.setUp()
        api = StubMeteoriteLandingsAPI()
        modelName = String(describing: type(of: self))
        dataStack = TestDataStack(modelName: modelName)
        storage = DefaultMeteoritesStorage(managedContext: dataStack.managedContext)
        repository = DefaultMeteoritesRepository(api: api, storage: storage)
    }

    func testFindMeteoritesWithinCircle() async throws {

        let meteoriteDTO = StubUtils.getMeteoriteDTOs().first!

        let meteorites = try await repository.findMeteorites(latitude: 1.0, longitude: 1.0, radius: 100)
        XCTAssertEqual(meteorites.count, 1)

        let meteorite = meteorites.first(where: { $0.id == meteoriteDTO.id })!
        XCTAssertEqual(meteorite.id, meteoriteDTO.id)
        XCTAssertEqual(meteorite.name, meteoriteDTO.name)
        XCTAssertEqual(meteorite.mass, meteoriteDTO.mass)
        XCTAssertEqual(meteorite.date, meteoriteDTO.date)
        XCTAssertEqual(meteorite.location.latitude, meteoriteDTO.location.latitude)
        XCTAssertEqual(meteorite.location.longitude, meteoriteDTO.location.longitude)
    }

    func testFindMeteoritesWithinBox() async throws {

        let meteorites = try await repository.findMeteorites(northwestLat: 1.0, northwestLong: 1.0, southeastLat: 2.0, southeastLong: 2.0)
        XCTAssertEqual(meteorites.count, 0)
    }

    func testEmptyMeteorites() throws {

        let meteorites = try repository.getMeteorites()
        XCTAssertEqual(meteorites.count, 0)
    }

    func testGetMeteorites() throws {

        let meteoriteEntity = try saveMeteoriteEntity()

        let meteorites = try repository.getMeteorites()
        XCTAssertEqual(meteorites.count, 1)

        let meteorite = meteorites.first(where: { $0.id == meteoriteEntity.id })!
        XCTAssertEqual(meteorite.id, meteoriteEntity.id)
        XCTAssertEqual(meteorite.name, meteoriteEntity.name)
        XCTAssertEqual(meteorite.mass, meteoriteEntity.mass)
        XCTAssertEqual(meteorite.date, meteoriteEntity.date)
        XCTAssertEqual(meteorite.location.latitude, meteoriteEntity.latitude)
        XCTAssertEqual(meteorite.location.longitude, meteoriteEntity.longitude)
    }

    func testSaveMeteorites() throws {

        let meteorites = StubUtils.getMeteorites()

        try repository.saveMeteorites(meteorites)
        let savedMeteorites = try repository.getMeteorites()
        XCTAssertEqual(meteorites.count, savedMeteorites.count)
    }

    override func tearDown() {

        super.tearDown()
    }

    private func saveMeteoriteEntity() throws -> MeteoriteEntity {

        let meteoriteEntity = MeteoriteEntity(context: dataStack.managedContext)
        meteoriteEntity.fromDomain(StubUtils.getMeteorite())
        try dataStack.managedContext.save()
        return meteoriteEntity
    }
}
