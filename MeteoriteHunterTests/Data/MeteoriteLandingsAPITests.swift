//
//  MeteoriteLandingsAPITests.swift
//  MeteoriteHunterTests
//
//  Created by Jan Pavlica on 03.02.2023.
//

import XCTest
@testable import Meteorite_Hunter

final class MeteoriteLandingsAPITests: XCTestCase {

    private let baseUrl = "https://example.com"
    private let appToken = "abcd1234"
    private let latitude = 1.0
    private let longitude = 1.0
    private let radius = 10

    private var networking: MockNetworking!
    private var api: MeteoriteLandingsAPI!

    override func setUp() {

        super.setUp()
        networking = MockNetworking()
        api = DefaultMeteoriteLandingsAPI(networking: networking, baseUrl: baseUrl, appToken: appToken)
    }

    func testRequestSuccess() async throws {

        let encoder = JSONEncoder()
        encoder.dateEncodingStrategy = .formatted(api.dateFormatter)

        let meteorites = StubUtils.getMeteoriteDTOs()
        networking.result = try .success(encoder.encode(meteorites))

        let result = try await api.getMeteoritesWithinCircle(latitude: latitude, longitude: longitude, radius: radius)
        XCTAssertEqual(meteorites.count, result.count)
    }

    func testInvalidUrl() async {

        api = DefaultMeteoriteLandingsAPI(networking: networking, baseUrl: "", appToken: appToken)
        do {
            _ = try await api.getMeteoritesWithinCircle(latitude: latitude, longitude: longitude, radius: radius)
        } catch {
            XCTAssertEqual(error as! NetworkError, NetworkError.invalidURL)
        }
    }

    func testNoResponseFail() async {

        networking.result = .failure(NetworkError.noResponse)
        do {
            _ = try await api.getMeteoritesWithinCircle(latitude: latitude, longitude: longitude, radius: radius)
        } catch {
            XCTAssertEqual(error as! NetworkError, NetworkError.noResponse)
        }
    }

    func testDecodeFail() async {

        do {
            networking.result = try .success(JSONEncoder().encode(String("")))
            _ = try await api.getMeteoritesWithinCircle(latitude: latitude, longitude: longitude, radius: radius)
        } catch {
            XCTAssertEqual(error as! NetworkError, NetworkError.decode)
        }
    }

    func testOfflineFail() async {

        networking.result = .failure(NetworkError.offline)
        do {
            _ = try await api.getMeteoritesWithinCircle(latitude: latitude, longitude: longitude, radius: radius)
        } catch {
            XCTAssertEqual(error as! NetworkError, NetworkError.offline)
        }
    }

    func testUnknownFail() async {

        networking.result = .failure(NetworkError.unknown)
        do {
            _ = try await api.getMeteoritesWithinCircle(latitude: latitude, longitude: longitude, radius: radius)
        } catch {
            XCTAssertEqual(error as! NetworkError, NetworkError.unknown)
        }
    }
}
