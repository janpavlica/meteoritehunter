//
//  ShotsRepositoryTests.swift
//  MeteoriteHunterTests
//
//  Created by Jan Pavlica on 06.02.2023.
//

import XCTest
@testable import Meteorite_Hunter

final class ShotsRepositoryTests: XCTestCase {

    private var suiteName: String!
    private var userDefaults: UserDefaults!
    private var storage: ShotsStorage!
    private var repository: ShotsRepository!

    override func setUp() {

        super.setUp()
        suiteName = String(describing: type(of: self))
        userDefaults = UserDefaults(suiteName: self.name)
        storage = DefaultShotsStorage(userDefaults: userDefaults)
        repository = DefaultShotsRepository(storage: storage)
    }

    func testExistsSuccessShot() {

        XCTAssertFalse(repository.existsSuccessShot())
        repository.addSuccess(count: 1)
        XCTAssertTrue(repository.existsSuccessShot())
    }

    func testExistsAnyShot() {

        XCTAssertFalse(repository.existsAnyShot())
        repository.addSuccess(count: 1)
        XCTAssertTrue(repository.existsAnyShot())
    }

    func testGetShots() {

        repository.addFailure()
        repository.addSuccess(count: 1)
        repository.addFailure()

        let shots = repository.getShots()
        XCTAssertEqual(shots.success, 1)
        XCTAssertEqual(shots.total, 3)
    }

    func testAddSuccess() {

        repository.addSuccess(count: 1)
        repository.addSuccess(count: 2)

        let shots = repository.getShots()
        XCTAssertEqual(shots.success, 3)
    }

    func testAddFailure() {

        repository.addFailure()
        repository.addFailure()

        let shots = repository.getShots()
        XCTAssertEqual(shots.success, 0)
        XCTAssertEqual(shots.total, 2)
    }

    func testClearShots() {

        repository.addSuccess(count: 1)
        repository.clearShots()
        let shots = repository.getShots()
        XCTAssertEqual(shots.total, 0)
    }

    override func tearDown() {

        repository.clearShots()
        super.tearDown()
    }
}
