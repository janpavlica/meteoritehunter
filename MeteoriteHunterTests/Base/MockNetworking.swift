//
//  MockNetworking.swift
//  MeteoriteHunterTests
//
//  Created by Jan Pavlica on 03.02.2023.
//

import Foundation
@testable import Meteorite_Hunter

class MockNetworking: Networking {

    var result = Result<Data, Error>.success(Data())

    func data(from url: URL, delegate: URLSessionTaskDelegate?) async throws -> (Data, URLResponse) {
        try (result.get(), HTTPURLResponse())
    }

    func data(for request: URLRequest, delegate: URLSessionTaskDelegate?) async throws -> (Data, URLResponse) {
        try (result.get(), HTTPURLResponse())
    }
}
