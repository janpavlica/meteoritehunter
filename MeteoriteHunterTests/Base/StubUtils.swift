//
//  MockUtils.swift
//  MeteoriteHunterTests
//
//  Created by Jan Pavlica on 04.02.2023.
//

import Foundation
@testable import Meteorite_Hunter

class StubUtils {

    static func getMeteoriteDTOs() -> [MeteoriteDTO] {

        let firstDate = getDateFromString("01.01.1984")!
        let firstLocation = LocationDTO(latitude: 1.0, longitude: 1.0)
        let firstMeteorite = MeteoriteDTO(id: 1, name: "First meteorite", recclass: "L5", mass: 100, date: firstDate, location: firstLocation)

        let secondDate = getDateFromString("01.01.1992")!
        let secondLocation = LocationDTO(latitude: 2.0, longitude: 2.0)
        let secondMeteorite = MeteoriteDTO(id: 2, name: "Second meteorite", recclass: "H6", mass: 1000, date: secondDate, location: secondLocation)

        return [ firstMeteorite, secondMeteorite ]
    }

    static func getMeteorites() -> [Meteorite] {

        return getMeteoriteDTOs().map { $0.toDomain() }
    }

    static func getMeteoriteEmptyDTOs() -> [MeteoriteDTO] {

        return []
    }

    static func getEmptyMeteorites() -> [Meteorite] {

        return getMeteoriteEmptyDTOs().map { $0.toDomain() }
    }

    static func getMeteoriteDTO() -> MeteoriteDTO {

        return getMeteoriteDTOs().first!
    }

    static func getMeteorite() -> Meteorite {

        return getMeteorites().first!
    }

    static func getDateFromString(_ string: String) -> Date? {

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy"
        return dateFormatter.date(from: string)
    }
}
