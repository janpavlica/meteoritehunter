//
//  MockMeteoriteLandingsAPI.swift
//  MeteoriteHunter
//
//  Created by Jan Pavlica on 24.01.2023.
//

import Foundation
@testable import Meteorite_Hunter

class StubMeteoriteLandingsAPI: MeteoriteLandingsAPI {

    var dateFormatter = DateFormatter()

    func getMeteoritesWithinCircle(latitude: Double, longitude: Double, radius: Int) async throws -> [MeteoriteDTO] {

        let meteorites = StubUtils.getMeteoriteDTOs()
        return [ meteorites.first! ]
    }

    func getMeteoritesWithinBox(northwestLat: Double, northwestLong: Double, southeastLat: Double,
                                southeastLong: Double) async throws -> [MeteoriteDTO] {
        return []
    }
}
