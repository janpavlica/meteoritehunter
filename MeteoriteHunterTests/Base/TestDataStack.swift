//
//  MockCoreDataStack.swift
//  MeteoriteHunterTests
//
//  Created by Jan Pavlica on 06.02.2023.
//

import Foundation
import CoreData
@testable import Meteorite_Hunter

class TestDataStack: CoreDataStack {

    override init(modelName: String) {

        super.init(modelName: modelName)

        let model = NSManagedObjectModel.mergedModel(from: [Bundle(for: type(of: self))])!
        let container = NSPersistentContainer(name: modelName, managedObjectModel: model)
        let description = NSPersistentStoreDescription()
        description.type = NSInMemoryStoreType
        description.shouldAddStoreAsynchronously = false

        container.persistentStoreDescriptions = [description]
        container.loadPersistentStores { (description, error) in
            precondition( description.type == NSInMemoryStoreType )
            if let error = error as NSError? {
                print("Unresolved error \(error), \(error.userInfo)")
            }
        }
        self.storeContainer = container
    }
}
