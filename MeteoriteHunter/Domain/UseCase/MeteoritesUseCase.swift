//
//  MeteoritesUseCase.swift
//  MeteoriteHunter
//
//  Created by Jan Pavlica on 24.01.2023.
//

import Foundation
import MapKit
import CoreLocation

struct MeteoritesUseCase {

    private let meteoritesRepository: MeteoritesRepository
    private let shotsRepository: ShotsRepository

    init(meteoritesRepository: MeteoritesRepository, shotsRepository: ShotsRepository) {

        self.meteoritesRepository = meteoritesRepository
        self.shotsRepository = shotsRepository
    }

    func findMeteorites(location: CLLocationCoordinate2D, radius: Int) async throws -> [Meteorite] {

        let meteoritesSaved = try meteoritesRepository.getMeteorites()
        let meteoritesFound = try await meteoritesRepository.findMeteorites(latitude: location.latitude,
                                                                            longitude: location.longitude, radius: radius)
        var meteorites = Array(Set(meteoritesFound).subtracting(Set(meteoritesSaved)))
        if meteorites.isEmpty {
            shotsRepository.addFailure()
        } else {
            meteorites = meteorites.map {
                var meteorite = $0
                let coordinate = CLLocationCoordinate2D(latitude: meteorite.location.latitude, longitude: meteorite.location.longitude)
                meteorite.searchDate = Date()
                meteorite.distance = MapUtils.calculateDistance(fromCoordinate: location, toCoordinate: coordinate)
                return meteorite
            }
            try meteoritesRepository.saveMeteorites(meteorites)
            shotsRepository.addSuccess(count: meteorites.count)
        }
        return meteorites
    }

    func findMeteorites(northwest: CLLocationCoordinate2D, southeast: CLLocationCoordinate2D) async throws -> [Meteorite] {

        let meteoritesSaved = try meteoritesRepository.getMeteorites()
        let meteoritesFound = try await meteoritesRepository.findMeteorites(northwestLat: northwest.latitude, northwestLong: northwest.longitude,
                                                             southeastLat: southeast.latitude, southeastLong: southeast.longitude)
        return Array(Set(meteoritesFound).subtracting(Set(meteoritesSaved)))
    }

    func getMeteorites() throws -> [Meteorite] {

        return try meteoritesRepository.getMeteorites()
    }

    func existsSuccessShot() -> Bool {

        return shotsRepository.existsSuccessShot()
    }

    func getShots() -> (success: Int, total: Int) {

        return shotsRepository.getShots()
    }
}
