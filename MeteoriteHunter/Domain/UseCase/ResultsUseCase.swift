//
//  ResultsUseCase.swift
//  MeteoriteHunter
//
//  Created by Jan Pavlica on 27.01.2023.
//

import Foundation

struct ResultsUseCase {

    private let meteoritesRepository: MeteoritesRepository
    private let shotsRepository: ShotsRepository

    init(meteoritesRepository: MeteoritesRepository, shotsRepository: ShotsRepository) {

        self.meteoritesRepository = meteoritesRepository
        self.shotsRepository = shotsRepository
    }

    func getMeteorites() throws -> [Meteorite] {

        let meteorites = try meteoritesRepository.getMeteorites()
        let sortedMeteorites = meteorites.sorted(by: {
            $0.searchDate?.compare($1.searchDate!) == .orderedDescending
        })
        return sortedMeteorites
    }

    func existsAnyShot() -> Bool {

        return shotsRepository.existsAnyShot()
    }

    func existsSuccessShot() -> Bool {

        return shotsRepository.existsSuccessShot()
    }

    func resetResults() throws {

        shotsRepository.clearShots()
        try meteoritesRepository.clearMeteorites()
    }
}
