//
//  Meteorite.swift
//  MeteoriteHunter
//
//  Created by Jan Pavlica on 23.01.2023.
//

import Foundation

struct Meteorite: Identifiable, Equatable, Hashable {

    let id: Int
    let name: String
    let mass: Double
    let landingDate: Date
    var searchDate: Date?
    let location: Location

    var radius: Int {
        Int(mass / 2)
    }

    var distance: Double?

    private let calendar = Calendar.current

    init(id: Int, name: String, mass: Double, landingDate: Date, location: Location) {

        self.id = id
        self.name = name
        self.mass = mass
        self.landingDate = landingDate
        self.location = location
    }

    init(id: Int, name: String, mass: Double, landingDate: Date, searchDate: Date, location: Location, distance: Double) {

        self.init(id: id, name: name, mass: mass, landingDate: landingDate, location: location)
        self.searchDate = searchDate
        self.distance = distance
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }

    static func == (lhs: Meteorite, rhs: Meteorite) -> Bool {
        lhs.id == rhs.id
    }
}
