//
//  Coordinate.swift
//  MeteoriteHunter
//
//  Created by Jan Pavlica on 23.01.2023.
//

import Foundation

struct Location {

    let latitude: Double
    let longitude: Double
}
