//
//  MeteoriteRepository.swift
//  MeteoriteHunter
//
//  Created by Jan Pavlica on 24.01.2023.
//

import Foundation

protocol MeteoritesRepository {

    func findMeteorites(latitude: Double, longitude: Double, radius: Int) async throws -> [Meteorite]
    func findMeteorites(northwestLat: Double, northwestLong: Double, southeastLat: Double, southeastLong: Double) async throws -> [Meteorite]
    func getMeteorites() throws -> [Meteorite]
    func saveMeteorites(_ meteorites: [Meteorite]) throws
    func clearMeteorites() throws
}
