//
//  ShotsRepository.swift
//  MeteoriteHunter
//
//  Created by Jan Pavlica on 27.01.2023.
//

import Foundation

protocol ShotsRepository {

    func existsSuccessShot() -> Bool
    func existsAnyShot() -> Bool
    func getShots() -> (success: Int, total: Int)
    func addSuccess(count: Int)
    func addFailure()
    func clearShots()
}
