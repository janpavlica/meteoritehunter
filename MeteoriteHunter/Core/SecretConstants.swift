//
//  SecretConstants.swift
//  MeteoriteHunter
//
//  Created by Jan Pavlica on 15.02.2023.
//

import Foundation

struct SecretsConstants {

    static let AppToken: [UInt8] = [9, 10, 16, 53, 12, 56, 70, 25, 14, 6, 49, 1, 65, 37, 61, 11, 8, 74, 120, 36, 54, 18, 42, 85, 3]
}
