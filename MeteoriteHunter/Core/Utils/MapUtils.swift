//
//  MapUtils.swift
//  MeteoriteHunter
//
//  Created by Jan Pavlica on 08.03.2023.
//

import MapKit

struct MapUtils {

    static func calculateDistance(fromCoordinate: CLLocationCoordinate2D, toCoordinate: CLLocationCoordinate2D) -> Double {

        let fromLocation = CLLocation(latitude: fromCoordinate.latitude, longitude: fromCoordinate.longitude)
        let toLocation = CLLocation(latitude: toCoordinate.latitude, longitude: toCoordinate.longitude)
        return fromLocation.distance(from: toLocation)
    }
}
