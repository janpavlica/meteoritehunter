//
//  Obfuscator.swift
//  MeteoriteHunter
//
//  Inspired by Dejan Atanasov
//  Created by Jan Pavlica on 15.02.2023.
//

import Foundation

class Obfuscator {

    private var salt: String

    init(withSalt salt: [AnyObject]) {

        self.salt = salt.description
    }

    func bytesByObfuscatingString(string: String) -> [UInt8] {

        let text = [UInt8](string.utf8)
        let cipher = [UInt8](self.salt.utf8)
        let length = cipher.count

        var encrypted = [UInt8]()

        for element in text.enumerated() {
            encrypted.append(element.element ^ cipher[element.offset % length])
        }

        #if DEBUG
        print("Salt: \(self.salt)")
        print("Original: \(string)")
        print("Encrypted: \(encrypted)\n")
        #endif

        return encrypted
    }

    func reveal(key: [UInt8]) -> String {

        let cipher = [UInt8](self.salt.utf8)
        let length = cipher.count

        var decrypted = [UInt8]()

        for key in key.enumerated() {
            decrypted.append(key.element ^ cipher[key.offset % length])
        }

        return String(bytes: decrypted, encoding: .utf8)!
    }
}
