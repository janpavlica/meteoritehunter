//
//  StringUtils.swift
//  MeteoriteHunter
//
//  Created by Jan Pavlica on 28.01.2023.
//

import Foundation

struct StringUtils {

    static func meteoritesSuffix(meteorites count: Int) -> String {

        switch count {
        case 0:
            return NSLocalizedString("moreMeteoritesSuffix", comment: "")
        case 1:
            return NSLocalizedString("oneMeteoriteSuffix", comment: "")
        case 2...4:
            return NSLocalizedString("twoMeteoritesSuffix", comment: "")
        default:
            return NSLocalizedString("moreMeteoritesSuffix", comment: "")
        }
    }
}
