//
//  DependencyContainer.swift
//  MeteoriteHunter
//
//  Created by Jan Pavlica on 29.01.2023.
//

import Swinject

final class DependencyContainer {

    static let shared = DependencyContainer()

    let container: Container = Container()
    let assembler: Assembler

    init() {
        assembler = Assembler(
            [
                DataSourceAssembly(),
                RepositoryAssembly(),
                UseCaseAssembly()
            ],
            container: container)
    }

    func resolve<T>() -> T {
        guard let resolvedType = container.resolve(T.self) else {
            fatalError()
        }
        return resolvedType
    }
}
