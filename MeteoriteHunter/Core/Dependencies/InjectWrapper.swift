//
//  InjectWrapper.swift
//  MeteoriteHunter
//
//  Created by Jan Pavlica on 29.01.2023.
//

import Foundation

@propertyWrapper
struct Inject<T> {

    let wrappedValue: T

    init() {
        wrappedValue = DependencyContainer.shared.resolve()
    }
}
