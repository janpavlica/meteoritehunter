//
//  UseCaseAssembly.swift
//  MeteoriteHunter
//
//  Created by Jan Pavlica on 29.01.2023.
//

import Swinject

final class UseCaseAssembly: Assembly {

    func assemble(container: Container) {

        container.register(MeteoritesUseCase.self) { resolver in

            guard let meteoritesRepository = resolver.resolve(MeteoritesRepository.self) else {
                fatalError("MeteoritesRepository dependency could not be resolved")
            }
            guard let shotsRepository = resolver.resolve(ShotsRepository.self) else {
                fatalError("ShotsRepository dependency could not be resolved")
            }
            return MeteoritesUseCase(meteoritesRepository: meteoritesRepository, shotsRepository: shotsRepository)
        }

        container.register(ResultsUseCase.self) { resolver in

            guard let meteoritesRepository = resolver.resolve(MeteoritesRepository.self) else {
                fatalError("MeteoritesRepository dependency could not be resolved")
            }
            guard let shotsRepository = resolver.resolve(ShotsRepository.self) else {
                fatalError("ShotsRepository dependency could not be resolved")
            }
            return ResultsUseCase(meteoritesRepository: meteoritesRepository, shotsRepository: shotsRepository)
        }
    }
}
