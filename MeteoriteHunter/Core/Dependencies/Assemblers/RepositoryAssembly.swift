//
//  RepositoryAssembly.swift
//  MeteoriteHunter
//
//  Created by Jan Pavlica on 29.01.2023.
//

import Swinject

final class RepositoryAssembly: Assembly {

    func assemble(container: Container) {

        container.register(MeteoritesRepository.self) { resolver in

            guard let api = resolver.resolve(MeteoriteLandingsAPI.self) else {
                fatalError("MeteoriteLandingsAPI dependency could not be resolved")
            }
            guard let storage = resolver.resolve(MeteoritesStorage.self) else {
                fatalError("MeteoritesStorage dependency could not be resolved")
            }
            return DefaultMeteoritesRepository(api: api, storage: storage)
        }

        container.register(ShotsRepository.self) { resolver in

            guard let storage = resolver.resolve(ShotsStorage.self) else {
                fatalError("ShotsStorage dependency could not be resolved")
            }
            return DefaultShotsRepository(storage: storage)
        }
    }
}
