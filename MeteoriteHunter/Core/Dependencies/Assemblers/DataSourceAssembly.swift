//
//  DataSourceAssembly.swift
//  MeteoriteHunter
//
//  Created by Jan Pavlica on 29.01.2023.
//

import Foundation
import Swinject

final class DataSourceAssembly: Assembly {

    func assemble(container: Container) {

        container.register(MeteoritesStorage.self) { _ in

            let context = AppDelegate.shared.coreDataStack.managedContext
            return DefaultMeteoritesStorage(managedContext: context)
        }

        container.register(ShotsStorage.self) { _ in

            return DefaultShotsStorage(userDefaults: UserDefaults.standard)
        }

        container.register(MeteoriteLandingsAPI.self) { _ in

            let obfuscator = Obfuscator(withSalt: [AppDelegate.self, NSObject.self, NSString.self])
            let token = obfuscator.reveal(key: SecretsConstants.AppToken)
            return DefaultMeteoriteLandingsAPI(appToken: token)
        }
    }
}
