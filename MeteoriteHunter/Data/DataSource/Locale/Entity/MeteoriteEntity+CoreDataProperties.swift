//
//  MeteoriteEntity+CoreDataProperties.swift
//  MeteoriteHunter
//
//  Created by Jan Pavlica on 27.01.2023.
//
//

import Foundation
import CoreData
import UIKit

extension MeteoriteEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<MeteoriteEntity> {
        return NSFetchRequest<MeteoriteEntity>(entityName: "MeteoriteEntity")
    }

    @NSManaged public var id: Int
    @NSManaged public var name: String
    @NSManaged public var mass: Double
    @NSManaged public var landingDate: Date
    @NSManaged public var searchDate: Date
    @NSManaged public var latitude: Double
    @NSManaged public var longitude: Double
    @NSManaged public var distance: Double

    func toDomain() -> Meteorite {
        .init(id: id, name: name, mass: mass, landingDate: landingDate, searchDate: searchDate,
              location: Location(latitude: latitude, longitude: longitude), distance: distance)
    }

    func fromDomain(_ domain: Meteorite) {
        id = domain.id
        name = domain.name
        mass = domain.mass
        landingDate = domain.landingDate
        searchDate = domain.searchDate ?? Date()
        latitude = domain.location.latitude
        longitude = domain.location.longitude
        distance = domain.distance ?? 0
    }
}

extension MeteoriteEntity: Identifiable {

}
