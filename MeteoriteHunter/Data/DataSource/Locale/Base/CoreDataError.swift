//
//  CoreDataError.swift
//  MeteoriteHunter
//
//  Created by Jan Pavlica on 27.01.2023.
//

import Foundation

enum CoreDataError: Error {

    case fetch
    case save
    case delete
}
