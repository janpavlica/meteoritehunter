//
//  DefaultMeteoritesStorage.swift
//  MeteoriteHunter
//
//  Created by Jan Pavlica on 24.01.2023.
//

import Foundation
import CoreData

final class DefaultMeteoritesStorage: MeteoritesStorage {

    private var managedContext: NSManagedObjectContext

    init(managedContext: NSManagedObjectContext) {

        self.managedContext = managedContext
    }

    func fetchMeteorites() throws -> [Meteorite] {

        let fetchRequest: NSFetchRequest<MeteoriteEntity> = MeteoriteEntity.fetchRequest()
        do {
            let meteorites = try managedContext.fetch(fetchRequest)
            return meteorites.map { $0.toDomain() }
        } catch {
            throw CoreDataError.fetch
        }
    }

    func saveMeteorites(_ meteorites: [Meteorite]) throws {

        for meteorite in meteorites {
            let entity = MeteoriteEntity(context: managedContext)
            entity.fromDomain(meteorite)
        }
        do {
            try managedContext.save()
        } catch {
            throw CoreDataError.save
        }
    }

    func deleteMeteorites() throws {

        let fetchRequest: NSFetchRequest<NSFetchRequestResult> = MeteoriteEntity.fetchRequest()
        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        batchDeleteRequest.resultType = .resultTypeObjectIDs
        do {
            try managedContext.executeAndMergeChanges(using: batchDeleteRequest)
        } catch {
            throw CoreDataError.delete
        }
    }
}

extension NSManagedObjectContext {

    public func executeAndMergeChanges(using batchDeleteRequest: NSBatchDeleteRequest) throws {

        batchDeleteRequest.resultType = .resultTypeObjectIDs
        let result = try execute(batchDeleteRequest) as? NSBatchDeleteResult
        let changes: [AnyHashable: Any] = [NSDeletedObjectsKey: result?.result as? [NSManagedObjectID] ?? []]
        NSManagedObjectContext.mergeChanges(fromRemoteContextSave: changes, into: [self])
    }
}
