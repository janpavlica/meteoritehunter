//
//  DefaultShotStorage.swift
//  MeteoriteHunter
//
//  Created by Jan Pavlica on 27.01.2023.
//

import Foundation

final class DefaultShotsStorage: ShotsStorage {

    private let successShotsKey = "meteorite.hunter.shots.success"
    private let totalShotsKey = "meteorite.hunter.shots.total"
    private var userDefaults: UserDefaults

    init(userDefaults: UserDefaults) {

        self.userDefaults = userDefaults
    }

    func getShots() -> (success: Int, total: Int) {

        let success = userDefaults.object(forKey: successShotsKey) as? Int ?? 0
        let total = userDefaults.object(forKey: totalShotsKey) as? Int ?? 0
        return (success: success, total: total)
    }

    func addSuccess(count: Int) {

        let shots = getShots()
        userDefaults.set(shots.success + count, forKey: successShotsKey)
        userDefaults.set(shots.total + 1, forKey: totalShotsKey)
    }

    func addFailure() {

        let shots = getShots()
        userDefaults.set(shots.total + 1, forKey: totalShotsKey)
    }

    func clearShots() {

        userDefaults.set(nil, forKey: successShotsKey)
        userDefaults.set(nil, forKey: totalShotsKey)
    }
}
