//
//  MeteoritesStorage.swift
//  MeteoriteHunter
//
//  Created by Jan Pavlica on 24.01.2023.
//

import Foundation

protocol MeteoritesStorage {

    func fetchMeteorites() throws -> [Meteorite]
    func saveMeteorites(_ meteorites: [Meteorite]) throws
    func deleteMeteorites() throws
}
