//
//  MeteoriteLandingsAPI.swift
//  MeteoriteHunter
//
//  Created by Jan Pavlica on 24.01.2023.
//

import Foundation

protocol MeteoriteLandingsAPI {

    var dateFormatter: DateFormatter { get }

    func getMeteoritesWithinCircle(latitude: Double, longitude: Double, radius: Int) async throws -> [MeteoriteDTO]
    func getMeteoritesWithinBox(northwestLat: Double, northwestLong: Double, southeastLat: Double,
                                southeastLong: Double) async throws -> [MeteoriteDTO]
}
