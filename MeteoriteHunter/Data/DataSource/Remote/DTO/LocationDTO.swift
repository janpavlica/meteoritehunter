//
//  LocationDTO.swift
//  MeteoriteHunter
//
//  Created by Jan Pavlica on 24.01.2023.
//

import Foundation

struct LocationDTO {

    let latitude: Double
    let longitude: Double
}

extension LocationDTO {

    func toDomain() -> Location {

        .init(latitude: latitude, longitude: longitude)
    }
}

extension LocationDTO: Decodable, Encodable {

    private enum CodingKeys: String, CodingKey {
        case latitude
        case longitude
    }

    init(from decoder: Decoder) throws {

        let container = try decoder.container(keyedBy: CodingKeys.self)
        latitude = Double(try container.decode(String.self, forKey: .latitude)) ?? 0
        longitude = Double(try container.decode(String.self, forKey: .longitude)) ?? 0
    }

    public func encode(to encoder: Encoder) throws {

        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(String(latitude), forKey: .latitude)
        try container.encode(String(longitude), forKey: .longitude)
    }
}
