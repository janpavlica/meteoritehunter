//
//  MeteoriteDTO.swift
//  MeteoriteHunter
//
//  Created by Jan Pavlica on 24.01.2023.
//

import Foundation

struct MeteoriteDTO {

    let id: Int
    let name: String
    let recclass: String
    let mass: Double
    let landingDate: Date
    let location: LocationDTO
}

extension MeteoriteDTO {

    func toDomain() -> Meteorite {
        .init(id: id, name: name, mass: mass, landingDate: landingDate, location: location.toDomain())
    }
}

extension MeteoriteDTO: Decodable, Encodable {

    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case recclass
        case mass
        case landingDate = "year"
        case location = "geolocation"
    }

    init(from decoder: Decoder) throws {

        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = Int(try container.decode(String.self, forKey: .id)) ?? 0
        name = try container.decode(String.self, forKey: .name)
        recclass = try container.decode(String.self, forKey: .recclass)
        mass = Double(try container.decode(String.self, forKey: .mass)) ?? 0
        landingDate = try container.decode(Date.self, forKey: .landingDate)
        location = try container.decode(LocationDTO.self, forKey: .location)
    }

    public func encode(to encoder: Encoder) throws {

        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(String(id), forKey: .id)
        try container.encode(name, forKey: .name)
        try container.encode(recclass, forKey: .recclass)
        try container.encode(String(mass), forKey: .mass)
        try container.encode(landingDate, forKey: .landingDate)
        try container.encode(location, forKey: .location)
    }
}
