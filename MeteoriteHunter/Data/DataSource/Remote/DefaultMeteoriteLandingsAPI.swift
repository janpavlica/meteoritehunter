//
//  DefaultMeteoriteLandingsAPI.swift
//  MeteoriteHunter
//
//  Created by Jan Pavlica on 24.01.2023.
//

import Foundation

final class DefaultMeteoriteLandingsAPI: NetworkService, MeteoriteLandingsAPI {

    private let networking: Networking
    private let baseUrl: String
    private let basePath: String

    var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        return formatter
    }()

    init(networking: Networking = URLSession.shared, baseUrl: String = "https://data.nasa.gov",
         basePath: String = "resource/gh4g-9sfh.json", appToken: String) {

        self.networking = networking
        self.baseUrl = baseUrl
        self.basePath = basePath

        let headers = ["X-App-Token": appToken]
        let config = NetworkConfiguration(url: baseUrl, headers: headers)

        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(dateFormatter)

        super.init(config: config, decoder: decoder)
    }

    func getMeteoritesWithinCircle(latitude: Double, longitude: Double, radius: Int) async throws -> [MeteoriteDTO] {

        let queryParameters = ["fall": "Found", "nametype": "Valid",
                               "$where": "within_circle(geolocation,\(latitude),\(longitude),\(radius))"]

        let endpoint = BaseEndpoint(path: basePath, method: .get, queryParameters: queryParameters, networking: networking)
        return try await sendRequest(endpoint: endpoint, responseModel: [MeteoriteDTO].self)
    }

    func getMeteoritesWithinBox(northwestLat: Double, northwestLong: Double, southeastLat: Double,
                                southeastLong: Double) async throws -> [MeteoriteDTO] {

        let queryParameters = ["fall": "Found", "nametype": "Valid",
                               "$where": "within_box(geolocation,\(northwestLat),\(northwestLong),\(southeastLat),\(southeastLong))"]

        let endpoint = BaseEndpoint(path: basePath, method: .get, queryParameters: queryParameters, networking: networking)
        return try await sendRequest(endpoint: endpoint, responseModel: [MeteoriteDTO].self)
    }
}
