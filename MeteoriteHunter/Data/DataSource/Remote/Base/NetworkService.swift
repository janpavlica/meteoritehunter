//
//  NetworkService.swift
//  MeteoriteHunter
//
//  Created by Jan Pavlica on 03.02.2023.
//

import Foundation

class NetworkService {

    private let config: NetworkConfigurable
    private let decoder: JSONDecoder

    init(config: NetworkConfigurable, decoder: JSONDecoder) {

        self.config = config
        self.decoder = decoder
    }

    func sendRequest<T: Decodable>(endpoint: Requestable, responseModel: T.Type) async throws -> T {

        let urlRequest = try endpoint.urlRequest(config: config)
        let data = try await endpoint.getData(request: urlRequest)
        // let json = String(decoding: data, as: UTF8.self)
        guard let decodedResponse = try? decoder.decode(responseModel, from: data) else {
            throw NetworkError.decode
        }
        return decodedResponse
    }
}
