//
//  BaseEndpoint.swift
//  MeteoriteHunter
//
//  Created by Jan Pavlica on 03.02.2023.
//

import Foundation

struct BaseEndpoint: Requestable {

    let path: String
    let method: RequestMethod
    let headerParameters: [String: String]
    let queryParameters: [String: Any]

    let networking: Networking

    init(path: String, method: RequestMethod, headerParameters: [String: String] = [:], queryParameters: [String: Any] = [:],
         networking: Networking = URLSession.shared) {

        self.path = path
        self.method = method
        self.headerParameters = headerParameters
        self.queryParameters = queryParameters
        self.networking = networking
    }

    func getData(request: URLRequest) async throws -> Data {

        do {
            let (data, response) = try await networking.data(for: request)
            guard let response = response as? HTTPURLResponse else {
                throw NetworkError.noResponse
            }
            switch response.statusCode {
            case 200...299:
                return data
            case 401:
                throw NetworkError.unauthorized
            default:
                throw NetworkError.unknown
            }
        } catch URLError.Code.notConnectedToInternet {
            throw NetworkError.offline
        }
    }
}
