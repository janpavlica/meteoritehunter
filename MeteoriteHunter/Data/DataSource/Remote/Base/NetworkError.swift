//
//  NetworkError.swift
//  MeteoriteHunter
//
//  Created by Jan Pavlica on 24.01.2023.
//

import Foundation

enum NetworkError: Error {

    case invalidURL
    case components
    case noResponse
    case decode
    case unauthorized
    case offline
    case unknown
}
