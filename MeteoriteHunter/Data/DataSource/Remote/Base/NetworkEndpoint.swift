//
//  NetworkEndpoint.swift
//  MeteoriteHunter
//
//  Created by Jan Pavlica on 03.02.2023.
//

import Foundation

protocol Requestable {

    var path: String { get }
    var method: RequestMethod { get }
    var headerParameters: [String: String] { get }
    var queryParameters: [String: Any] { get }

    var networking: Networking { get }

    func urlRequest(config: NetworkConfigurable) throws -> URLRequest
    func getData(request: URLRequest) async throws -> Data
}

extension Requestable {

    func urlRequest(config: NetworkConfigurable) throws -> URLRequest {

        let url = try url(config: config)
        var urlRequest = URLRequest(url: url)
        var allHeaders: [String: String] = config.headers
        headerParameters.forEach { allHeaders.updateValue($1, forKey: $0) }

        urlRequest.allHTTPHeaderFields = allHeaders
        urlRequest.httpMethod = method.rawValue
        return urlRequest
    }

    func url(config: NetworkConfigurable) throws -> URL {

        if config.url.isEmpty {
            throw NetworkError.invalidURL
        }

        let baseURL = config.url.last != "/" ? "\(config.url)/" : config.url
        let endpoint = baseURL.appending(path)

        guard var urlComponents = URLComponents(string: endpoint) else { throw NetworkError.components }

        var urlQueryItems = [URLQueryItem]()
        queryParameters.forEach {
            urlQueryItems.append(URLQueryItem(name: $0.key, value: "\($0.value)"))
        }
        urlComponents.queryItems = !urlQueryItems.isEmpty ? urlQueryItems : nil

        guard let url = urlComponents.url else { throw NetworkError.invalidURL }
        return url
    }
}

enum RequestMethod: String {

    case get = "GET"
    case head = "HEAD"
    case post = "POST"
    case put = "PUT"
    case patch = "PATCH"
    case delete = "DELETE"
}
