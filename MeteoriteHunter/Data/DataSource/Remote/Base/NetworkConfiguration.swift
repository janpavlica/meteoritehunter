//
//  NetworkConfiguration.swift
//  MeteoriteHunter
//
//  Created by Jan Pavlica on 03.02.2023.
//

import Foundation

protocol NetworkConfigurable {

    var url: String { get }
    var headers: [String: String] { get }
}

struct NetworkConfiguration: NetworkConfigurable {

    let url: String
    let headers: [String: String]

    init(url: String, headers: [String: String] = [:]) {

        self.url = url
        self.headers = headers
    }
}
