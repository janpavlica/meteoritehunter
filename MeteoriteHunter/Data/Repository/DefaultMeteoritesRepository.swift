//
//  DefaultMeteoritesRepository.swift
//  MeteoriteHunter
//
//  Created by Jan Pavlica on 24.01.2023.
//

import Foundation

final class DefaultMeteoritesRepository: MeteoritesRepository {

    private let api: MeteoriteLandingsAPI
    private let storage: MeteoritesStorage

    init(api: MeteoriteLandingsAPI, storage: MeteoritesStorage) {
        self.api = api
        self.storage = storage
    }

    func findMeteorites(latitude: Double, longitude: Double, radius: Int) async throws -> [Meteorite] {

        let meteorites = try await api.getMeteoritesWithinCircle(latitude: latitude, longitude: longitude, radius: radius)
        return meteorites.map { $0.toDomain() }
    }

    func findMeteorites(northwestLat: Double, northwestLong: Double, southeastLat: Double, southeastLong: Double) async throws -> [Meteorite] {

        let meteorites = try await api.getMeteoritesWithinBox(northwestLat: northwestLat, northwestLong: northwestLong, southeastLat: southeastLat,
                                                              southeastLong: southeastLong)
        return meteorites.map { $0.toDomain() }
    }

    func getMeteorites() throws -> [Meteorite] {

        return try storage.fetchMeteorites()
    }

    func saveMeteorites(_ meteorites: [Meteorite]) throws {

        try storage.saveMeteorites(meteorites)
    }

    func clearMeteorites() throws {

        try storage.deleteMeteorites()
    }
}
