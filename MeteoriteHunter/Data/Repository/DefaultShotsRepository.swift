//
//  File.swift
//  MeteoriteHunter
//
//  Created by Jan Pavlica on 27.01.2023.
//

import Foundation

final class DefaultShotsRepository: ShotsRepository {

    private let storage: ShotsStorage

    init(storage: ShotsStorage) {
        self.storage = storage
    }

    func existsSuccessShot() -> Bool {

        return storage.getShots().success > 0
    }

    func existsAnyShot() -> Bool {

        return storage.getShots().total > 0
    }

    func getShots() -> (success: Int, total: Int) {

        return storage.getShots()
    }

    func addSuccess(count: Int) {

        storage.addSuccess(count: count)
    }

    func addFailure() {

        storage.addFailure()
    }

    func clearShots() {

        storage.clearShots()
    }
}
