//
//  UILabel+Extension.swift
//  MeteoriteHunter
//
//  Created by Jan Pavlica on 07.03.2023.
//

import UIKit

extension UILabel {

    func setParagraphStyle(alignment: NSTextAlignment = .left, lineSpacing: CGFloat = 2) {

        guard let textString = text else { return }

        let attributedString = NSMutableAttributedString(string: textString)

        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = lineSpacing
        paragraphStyle.alignment = alignment

        attributedString.addAttribute(
            .paragraphStyle,
            value: paragraphStyle,
            range: NSRange(location: 0, length: attributedString.length
        ))

        attributedText = attributedString
    }
}
