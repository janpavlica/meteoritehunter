//
//  IntroductionViewModel.swift
//  MeteoriteHunter
//
//  Created by Jan Pavlica on 23.01.2023.
//

import Foundation

final class IntroductionViewModel {

    let titleText = NSLocalizedString("introTitle", comment: "")
    let descriptionText = NSLocalizedString("introDescription", comment: "")
    let startText = NSLocalizedString("introStart", comment: "")
}
