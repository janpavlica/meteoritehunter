//
//  IntroductionCoordinator.swift
//  MeteoriteHunter
//
//  Created by Jan Pavlica on 20.01.2023.
//

import UIKit

protocol IntroductionCoordinatorFlow: Coordinator {

    func showIntroductionView()
    func showMainView()
}

class IntroductionCoordinator: IntroductionCoordinatorFlow {

    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController

    required init(_ navigationController: UINavigationController) {

        self.navigationController = navigationController
    }

    func start() {

        showIntroductionView()
    }

    func showIntroductionView() {

        let introductionView = IntroductionViewController()
        introductionView.coordinator = self
        navigationController.setViewControllers([introductionView], animated: false)
    }

    func showMainView() {
        let mainCoordinator = MainCoordinator(navigationController)
        mainCoordinator.start()
        childCoordinators.append(mainCoordinator)
    }

    deinit {
        print("IntroductionCoordinator deinit")
    }
}
