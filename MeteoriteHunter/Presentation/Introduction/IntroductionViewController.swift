//
//  IntroductionViewController.swift
//  MeteoriteHunter
//
//  Created by Jan Pavlica on 20.01.2023.
//

import UIKit

class IntroductionViewController: UIViewController {

    var coordinator: IntroductionCoordinatorFlow?
    private let viewModel: IntroductionViewModel

    lazy private var backgroundImage: UIImageView = {
        let view = UIImageView(image: UIImage(named: "planet-earth-clear"))
        view.layer.opacity = 0.1
        return view
    }()

    lazy private var titleLabel: TitleLabel = {
        let label = TitleLabel()
        return label
    }()

    lazy private var introImage: UIImageView = {
        let view = UIImageView(image: UIImage(named: "planet-earth"))
        return view
    }()

    lazy private var descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.appFont(.regular, size: 19)
        label.textColor = UIColor.appColor(.grayText)
        label.textAlignment = .center
        label.numberOfLines = .zero
        return label
    }()

    lazy private var startButton: BaseButton = {
        let button = BaseButton()
        button.baseBackgroundColor = UIColor.appColor(.persianGreen)
        button.addTarget(self, action: #selector(start(_:)), for: .touchUpInside)
        return button
    }()

    init(viewModel: IntroductionViewModel = IntroductionViewModel()) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {

        super.viewDidLoad()
        setupView()
        addSubViews()
        setupConstraints()
    }

    private func setupView() {

        titleLabel.text = viewModel.titleText
        descriptionLabel.text = viewModel.descriptionText
        startButton.setTitle(viewModel.startText, for: .normal)
        view.backgroundColor = .systemBackground
    }

    private func addSubViews() {

        let subviews = [backgroundImage, titleLabel, introImage, descriptionLabel, startButton]
        subviews.forEach {
            self.view.addSubview($0)
            $0.translatesAutoresizingMaskIntoConstraints = false
        }
    }

    private func setupConstraints() {

        let safeGuide = view.safeAreaLayoutGuide

        titleLabel.topAnchor.constraint(equalTo: safeGuide.topAnchor, constant: 30).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: safeGuide.leadingAnchor, constant: 20).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: safeGuide.trailingAnchor, constant: -20).isActive = true
        titleLabel.heightAnchor.constraint(equalToConstant: 100).isActive = true

        introImage.widthAnchor.constraint(equalToConstant: 200).isActive = true
        introImage.heightAnchor.constraint(equalToConstant: 200).isActive = true
        introImage.centerXAnchor.constraint(equalTo: safeGuide.centerXAnchor).isActive = true
        introImage.centerYAnchor.constraint(equalTo: safeGuide.centerYAnchor, constant: -120).isActive = true

        backgroundImage.widthAnchor.constraint(equalToConstant: view.bounds.height).isActive = true
        backgroundImage.heightAnchor.constraint(equalToConstant: view.bounds.height).isActive = true
        backgroundImage.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        backgroundImage.topAnchor.constraint(equalTo: introImage.centerYAnchor).isActive = true

        descriptionLabel.topAnchor.constraint(equalTo: introImage.bottomAnchor, constant: 50).isActive = true
        descriptionLabel.leadingAnchor.constraint(equalTo: safeGuide.leadingAnchor, constant: 30).isActive = true
        descriptionLabel.trailingAnchor.constraint(equalTo: safeGuide.trailingAnchor, constant: -30).isActive = true

        startButton.leadingAnchor.constraint(equalTo: safeGuide.leadingAnchor, constant: 20).isActive = true
        startButton.trailingAnchor.constraint(equalTo: safeGuide.trailingAnchor, constant: -20).isActive = true
        startButton.bottomAnchor.constraint(equalTo: safeGuide.bottomAnchor, constant: -20).isActive = true
    }

    @objc private func start(_ sender: UIButton?) {
        coordinator?.showMainView()
    }
}
