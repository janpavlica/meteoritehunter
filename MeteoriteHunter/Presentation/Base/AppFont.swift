//
//  AppFont.swift
//  MeteoriteHunter
//
//  Created by Jan Pavlica on 22.02.2023.
//

import UIKit

enum AppFont: String {
    case regular = "Rubik-Regular"
    case medium = "Rubik-Medium"
    case semibold = "Rubik-SemiBold"
    case bold = "Rubik-Bold"
    case black = "Bungee-Regular"
}

extension UIFont {

    static func appFont(_ name: AppFont, size: CGFloat) -> UIFont? {
        return UIFont(name: name.rawValue, size: size)
    }
}
