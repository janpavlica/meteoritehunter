//
//  CustomColor.swift
//  MeteoriteHunter
//
//  Created by Jan Pavlica on 20.01.2023.
//

import UIKit

enum AppColor: String {
    case black
    case white
    case charcoal
    case charcoalText
    case persianGreen
    case maizeCrayola
    case sandyBrown
    case burntSienna
    case grayText
}

extension UIColor {

    static func appColor(_ name: AppColor) -> UIColor? {
        return UIColor(named: name.rawValue)
    }
}
