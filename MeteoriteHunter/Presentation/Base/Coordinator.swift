//
//  Coordinator.swift
//  MeteoriteHunter
//
//  Created by Jan Pavlica on 20.01.2023.
//

import UIKit

protocol Coordinator {

    var childCoordinators: [Coordinator] { get set }
    var navigationController: UINavigationController { get set }

    init(_ navigationController: UINavigationController)

    func start()
}
