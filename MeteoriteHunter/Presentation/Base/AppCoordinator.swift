//
//  AppCoordinator.swift
//  MeteoriteHunter
//
//  Created by Jan Pavlica on 20.01.2023.
//

import UIKit

protocol AppCoordinatorFlow: Coordinator {

    func showIntroductionView()
    func showMainView()
}

class AppCoordinator: AppCoordinatorFlow {

    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController

    @Inject private var useCase: ResultsUseCase

    required init(_ navigationController: UINavigationController) {

        self.navigationController = navigationController
        navigationController.setNavigationBarHidden(true, animated: true)
    }

    func start() {

        if useCase.existsSuccessShot() {
            showMainView()
        } else {
            showIntroductionView()
        }
    }

    func showIntroductionView() {

        let introductionCoordinator = IntroductionCoordinator(navigationController)
        introductionCoordinator.start()
        childCoordinators.append(introductionCoordinator)
    }

    func showMainView() {

        let mainCoordinator = MainCoordinator(navigationController)
        mainCoordinator.start()
        childCoordinators.append(mainCoordinator)
    }

    deinit {
        print("AppCoordinator deinit")
    }
}
