//
//  MainViewModel.swift
//  MeteoriteHunter
//
//  Created by Jan Pavlica on 23.01.2023.
//

import Foundation
import Combine
import MapKit
import CoreLocation

enum MainViewModelState: Equatable {
    case searching
    case loading
    case empty([MKPointAnnotation], [CraterCircleView])
    case finished([Meteorite], Bool)
    case error(String)
}

final class MainViewModel: NSObject {

    let titleText = NSLocalizedString("mainTitle", comment: "")
    let searchMeteoritesText = NSLocalizedString("mainSearchMeteorites", comment: "")
    let searchingMeteoritesText = NSLocalizedString("mainSearchingMeteorites", comment: "")
    let enterLocationText = NSLocalizedString("mainEnterLocation", comment: "")
    let radiusDescriptionText = NSLocalizedString("radiusDescriptionText", comment: "")
    let scoreDescriptionText = NSLocalizedString("scoreDescriptionText", comment: "")

    @Published private(set) var annotations: [MKPointAnnotation] = []
    @Published private(set) var craters: [CraterCircleView] = []
    @Published private(set) var annotationsHelp: [MKPointAnnotation] = []
    @Published private(set) var cratersHelp: [CraterCircleView] = []
    @Published private(set) var visibleResults: Bool = false
    @Published private(set) var state: MainViewModelState = .searching
    @Published private(set) var locationCoordinate: CLLocationCoordinate2D = CLLocationCoordinate2D()
    @Published private(set) var radiusInKm: String = ""
    @Published private(set) var shots: String = ""

    @Inject private var useCase: MeteoritesUseCase
    private var radius: Double = 0
    private var cancellable: AnyCancellable?

    private var numberFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.locale = Locale.current
        formatter.numberStyle = .decimal
        formatter.maximumFractionDigits = 2
        formatter.positiveSuffix = " km"
        return formatter
    }()

    lazy private var locationManager: CLLocationManager = {
        let manager = CLLocationManager()
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.delegate = self
        return manager
    }()

    func updateLocation() {
        switch locationManager.authorizationStatus {
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
        case .authorizedWhenInUse, .authorizedAlways:
            locationManager.startUpdatingLocation()
        default:
            self.state = .error("Location disabled")
        }
    }

    func updateLocation(by location: String) {
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(location) { (placemarks, _) in
            guard let placemarks = placemarks, let location = placemarks.first?.location else {
                self.state = .error("Location not found")
                return
            }
            self.locationCoordinate = location.coordinate
        }
    }

    func loadMeteorites() {
        clearHelp()
        loadResults()

        state = .loading
        cancellable = getMeteorites()
            .receive(on: DispatchQueue.main)
            .sink { completion in
                switch completion {
                case .failure(let error):
                    self.state = .error(error.localizedDescription)
                default: break
                }
            } receiveValue: { meteorites in
                if meteorites.isEmpty {
                    self.state = .empty(self.annotations, self.craters)
                    self.annotations.removeAll()
                    self.craters.removeAll()
                } else {
                    self.state = .finished(meteorites, false)
                    self.annotations = self.getAnnotations(for: meteorites)
                    self.craters = self.getCraters(for: meteorites, with: UIColor.appColor(AppColor.sandyBrown))
                }
            }
    }

    func findMeteorites(location: CLLocationCoordinate2D) {
        clearHelp()

        state = .loading
        cancellable = findMeteorites(location: location)
            .receive(on: DispatchQueue.main)
            .sink { completion in
                switch completion {
                case .failure(let error):
                    self.state = .error(error.localizedDescription)
                default: break
                }
            } receiveValue: { meteorites in
                self.state = .finished(meteorites, true)
                self.annotations.append(contentsOf: self.getAnnotations(for: meteorites))
                self.craters.append(contentsOf: self.getCraters(for: meteorites, with: UIColor.appColor(AppColor.sandyBrown)))
                self.loadResults()
            }
    }

    func findMeteorites(rectangle: MKMapRect) {
        clearHelp()

        state = .loading
        let southeastPoint = MKMapPoint(x: rectangle.origin.x + rectangle.size.width, y: rectangle.origin.y + rectangle.size.height)
        cancellable = findMeteorites(northwest: rectangle.origin.coordinate, southeast: southeastPoint.coordinate)
            .receive(on: DispatchQueue.main)
            .sink { completion in
                switch completion {
                case .failure(let error):
                    self.state = .error(error.localizedDescription)
                default: break
                }
            } receiveValue: { meteorites in
                self.state = .finished(meteorites, false)
                self.annotationsHelp = self.getAnnotations(for: meteorites)
                self.cratersHelp = self.getCraters(for: meteorites, with: UIColor.appColor(AppColor.maizeCrayola))
            }
    }

    func calculateRadius(center: CLLocationCoordinate2D, margin: CLLocationCoordinate2D) {
        radius = MapUtils.calculateDistance(fromCoordinate: center, toCoordinate: margin)

        guard let radiusText = numberFormatter.string(from: (radius / 1000) as NSNumber) else { return }
        radiusInKm = radiusText
    }

    private func loadResults() {
        let shotsScore = useCase.getShots()
        shots = "\(shotsScore.success) / \(shotsScore.total)"
        visibleResults = useCase.existsSuccessShot()
    }

    private func getAnnotations(for meteorites: [Meteorite]) -> [MKPointAnnotation] {
        return meteorites.map {
            let coordinate = CLLocationCoordinate2D(latitude: $0.location.latitude, longitude: $0.location.longitude)
            let annotation = MKPointAnnotation()
            annotation.coordinate = coordinate
            annotation.title = $0.name
            return annotation
        }
    }

    private func getCraters(for meteorites: [Meteorite], with color: UIColor?) -> [CraterCircleView] {
        return meteorites.map {
            let coordinate = CLLocationCoordinate2D(latitude: $0.location.latitude, longitude: $0.location.longitude)
            let crater = CraterCircleView(center: coordinate, radius: CLLocationDistance($0.radius))
            crater.color = color ?? .gray
            return crater
        }
    }

    private func clearHelp() {
        state = .empty(self.annotationsHelp, self.cratersHelp)
        annotationsHelp.removeAll()
        cratersHelp.removeAll()
    }
}

extension MainViewModel: CLLocationManagerDelegate {

    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        switch manager.authorizationStatus {
        case .authorizedWhenInUse, .authorizedAlways:
            manager.startUpdatingLocation()
        default:
            manager.stopUpdatingLocation()
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else { return }
        self.locationCoordinate = location.coordinate
        manager.stopUpdatingLocation()
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        self.state = .error(error.localizedDescription)
    }
}

extension MainViewModel {

    private func getMeteorites() -> Future<[Meteorite], Error> {
        return Future({ [self] promise in
            Task {
                do {
                    let meteorites = try useCase.getMeteorites()
                    promise(.success(meteorites))
                } catch {
                    promise(.failure(error))
                }
            }
        })
    }

    private func findMeteorites(location: CLLocationCoordinate2D) -> Future<[Meteorite], Error> {
        return Future({ [self] promise in
            Task {
                do {
                    let meteorites = try await useCase.findMeteorites(location: location, radius: Int(radius))
                    promise(.success(meteorites))
                } catch {
                    promise(.failure(error))
                }
            }
        })
    }

    private func findMeteorites(northwest: CLLocationCoordinate2D, southeast: CLLocationCoordinate2D) -> Future<[Meteorite], Error> {
        return Future({ [self] promise in
            Task {
                do {
                    let meteorites = try await useCase.findMeteorites(northwest: northwest, southeast: southeast)
                    promise(.success(meteorites))
                } catch {
                    promise(.failure(error))
                }
            }
        })
    }
}
