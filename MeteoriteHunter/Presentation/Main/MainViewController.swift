//
//  MainViewController.swift
//  MeteoriteHunter
//
//  Created by Jan Pavlica on 20.01.2023.
//

import UIKit
import Combine
import MapKit
import CoreLocation

class MainViewController: UIViewController {

    var coordinator: MainCoordinatorFlow?

    private let viewModel: MainViewModel
    private var cancellable = Set<AnyCancellable>()

    lazy private var mapView: MKMapView = {
        let view = MKMapView()
        view.delegate = self
        view.mapType = MKMapType.mutedStandard
        view.isRotateEnabled = false
        view.isZoomEnabled = true
        view.isScrollEnabled = true
        view.layoutMargins = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        return view
    }()

    lazy private var titleLabel: TitleLabel = {
        let label = TitleLabel()
        return label
    }()

    lazy private var targetImage: UIImageView = {
        let configuration = UIImage.SymbolConfiguration(pointSize: 120, weight: .thin, scale: .large)
        let image = UIImage(systemName: "viewfinder.circle", withConfiguration: configuration)
        let view = UIImageView(image: image)
        view.tintColor = UIColor.appColor(AppColor.charcoalText)
        return view
    }()

    lazy private var locationButton: IconButton = {
        let button = IconButton()
        button.icon = UIImage(systemName: "location.fill")
        button.baseBackgroundColor = UIColor.appColor(AppColor.charcoal)
        button.addTarget(self, action: #selector(setActualLocation(_:)), for: .touchUpInside)
        return button
    }()

    lazy private var searchButton: ExpandableSearchButton = {
        let button = ExpandableSearchButton()
        button.delegate = self
        return button
    }()

    lazy private var helpButton: IconButton = {
        let button = IconButton()
        button.icon = UIImage(systemName: "eye.fill")
        button.baseBackgroundColor = UIColor.appColor(AppColor.maizeCrayola)
        button.addTarget(self, action: #selector(showHelp(_:)), for: .touchUpInside)
        return button
    }()

    lazy private var resultsButton: IconButton = {
        let button = IconButton()
        button.icon = UIImage(systemName: "square.stack.3d.up.fill")
        button.baseBackgroundColor = UIColor.appColor(AppColor.burntSienna)
        button.addTarget(self, action: #selector(showResults(_:)), for: .touchUpInside)
        return button
    }()

    lazy private var radiusCounter: BaseCounter = {
        let counter = BaseCounter()
        return counter
    }()

    lazy private var scoreCounter: BaseCounter = {
        let counter = BaseCounter()
        counter.alignment = .right
        return counter
    }()

    lazy private var findButton: BaseButton = {
        let button = BaseButton()
        button.baseBackgroundColor = UIColor.appColor(AppColor.sandyBrown)
        button.addTarget(self, action: #selector(findMeteorites(_:)), for: .touchUpInside)
        return button
    }()

    init(viewModel: MainViewModel = MainViewModel()) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
        addSubViews()
        setupConstraints()
        setupBindings()

        viewModel.updateLocation()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.loadMeteorites()
    }

    @objc private func setActualLocation(_ sender: UIButton?) {
        viewModel.updateLocation()
    }

    @objc private func showResults(_ sender: UIButton?) {
        coordinator?.showResultsView(resetCompletion: {
            self.viewModel.loadMeteorites()
        })
    }

    @objc private func showHelp(_ sender: UIButton?) {
        viewModel.findMeteorites(rectangle: mapView.visibleMapRect)
    }

    @objc private func findMeteorites(_ sender: UIButton?) {
        viewModel.findMeteorites(location: mapView.centerCoordinate)
    }

    private func setupView() {
        titleLabel.text = viewModel.titleText
        searchButton.placeholderText = viewModel.enterLocationText
        radiusCounter.name = viewModel.radiusDescriptionText
        scoreCounter.name = viewModel.scoreDescriptionText

        #if DEBUG
        helpButton.isHidden = false
        #else
        helpButton.isHidden = true
        #endif

        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.hideKeyboard))
        mapView.addGestureRecognizer(tapRecognizer)
    }

    @objc private func hideKeyboard() {
        searchButton.isExpanded = false
    }

    private func addSubViews() {
        let subviews = [mapView, titleLabel, targetImage, locationButton, searchButton,
                        helpButton, resultsButton, radiusCounter, scoreCounter, findButton]
        subviews.forEach {
            self.view.addSubview($0)
            $0.translatesAutoresizingMaskIntoConstraints = false
        }
    }

    private func setupConstraints() {
        let safeGuide = view.safeAreaLayoutGuide

        mapView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        mapView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        mapView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        mapView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true

        titleLabel.topAnchor.constraint(equalTo: safeGuide.topAnchor, constant: 20).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: safeGuide.leadingAnchor, constant: 20).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: safeGuide.trailingAnchor, constant: -20).isActive = true
        titleLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true

        locationButton.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 30).isActive = true
        locationButton.trailingAnchor.constraint(equalTo: safeGuide.trailingAnchor, constant: -20).isActive = true

        searchButton.topAnchor.constraint(equalTo: locationButton.bottomAnchor, constant: 20).isActive = true
        searchButton.leadingAnchor.constraint(equalTo: safeGuide.leadingAnchor, constant: 20).isActive = true
        searchButton.trailingAnchor.constraint(equalTo: safeGuide.trailingAnchor, constant: -20).isActive = true

        helpButton.topAnchor.constraint(equalTo: searchButton.bottomAnchor, constant: 20).isActive = true
        helpButton.trailingAnchor.constraint(equalTo: safeGuide.trailingAnchor, constant: -20).isActive = true

        resultsButton.bottomAnchor.constraint(equalTo: scoreCounter.topAnchor, constant: -20).isActive = true
        resultsButton.trailingAnchor.constraint(equalTo: safeGuide.trailingAnchor, constant: -20).isActive = true

        targetImage.centerXAnchor.constraint(equalTo: safeGuide.centerXAnchor).isActive = true
        targetImage.centerYAnchor.constraint(equalTo: safeGuide.centerYAnchor).isActive = true

        radiusCounter.bottomAnchor.constraint(equalTo: findButton.topAnchor, constant: -30).isActive = true
        radiusCounter.leadingAnchor.constraint(equalTo: safeGuide.leadingAnchor, constant: 20).isActive = true
        radiusCounter.trailingAnchor.constraint(greaterThanOrEqualTo: scoreCounter.leadingAnchor, constant: 20).isActive = true

        scoreCounter.bottomAnchor.constraint(equalTo: findButton.topAnchor, constant: -30).isActive = true
        scoreCounter.trailingAnchor.constraint(equalTo: safeGuide.trailingAnchor, constant: -20).isActive = true

        findButton.leadingAnchor.constraint(equalTo: safeGuide.leadingAnchor, constant: 20).isActive = true
        findButton.trailingAnchor.constraint(equalTo: safeGuide.trailingAnchor, constant: -20).isActive = true
        findButton.bottomAnchor.constraint(equalTo: safeGuide.bottomAnchor, constant: -20).isActive = true
    }

    private func setupBindings() {
        viewModel.$annotations
            .receive(on: RunLoop.main)
            .sink(receiveValue: { annotations in
                if !annotations.isEmpty {
                    self.mapView.addAnnotations(annotations)
                }
            })
            .store(in: &cancellable)

        viewModel.$craters
            .receive(on: RunLoop.main)
            .sink(receiveValue: { craters in
                if !craters.isEmpty {
                    self.mapView.addOverlays(craters)
                }
            })
            .store(in: &cancellable)

        viewModel.$annotationsHelp
            .receive(on: RunLoop.main)
            .sink(receiveValue: { annotations in
                if !annotations.isEmpty {
                    self.mapView.addAnnotations(annotations)
                    self.showAnnotations(annotations)
                }
            })
            .store(in: &cancellable)

        viewModel.$cratersHelp
            .receive(on: RunLoop.main)
            .sink(receiveValue: { craters in
                if !craters.isEmpty {
                    self.mapView.addOverlays(craters)
                }
            })
            .store(in: &cancellable)

        viewModel.$locationCoordinate
            .receive(on: RunLoop.main)
            .sink(receiveValue: { coordinate in
                self.showLocationRegion(coordinate: coordinate)
            })
            .store(in: &cancellable)

        viewModel.$radiusInKm
            .receive(on: RunLoop.main)
            .sink(receiveValue: { radius in
                self.radiusCounter.value = radius
            })
            .store(in: &cancellable)

        let stateValueHandler: (MainViewModelState) -> Void = { [weak self] state in
            switch state {
            case .loading:
                self?.toggleButtons(false)
            case .searching:
                self?.toggleButtons(true)
            case .empty(let annotations, let craters):
                self?.mapView.removeAnnotations(annotations)
                self?.mapView.removeOverlays(craters)
                self?.toggleButtons(true)
            case .finished(let meteorites, let notification):
                if notification {
                    self?.coordinator?.showAttemptResultView(with: meteorites)
                }
                self?.toggleButtons(true)
            case .error(let error):
                self?.toggleButtons(true)
                print(error)
            }
        }

        viewModel.$state
                .receive(on: RunLoop.main)
                .sink(receiveValue: stateValueHandler)
                .store(in: &cancellable)

        viewModel.$visibleResults
            .receive(on: RunLoop.main)
            .sink(receiveValue: { visible in
                self.resultsButton.isHidden = !visible
            })
            .store(in: &cancellable)

        viewModel.$shots
            .receive(on: RunLoop.main)
            .sink(receiveValue: { score in
                self.scoreCounter.value = score
            })
            .store(in: &cancellable)
    }

    private func showAnnotations(_ annotations: [MKPointAnnotation]) {
        UIView.animate(withDuration: 0.5, animations: {

            if annotations.count > 1 {

                self.mapView.showAnnotations(annotations, animated: true)

            } else {

                guard let annotation = annotations.first else { return }
                self.showLocationRegion(coordinate: annotation.coordinate)
            }
        })
    }

    private func toggleButtons(_ isEnabled: Bool) {
        locationButton.isUserInteractionEnabled = isEnabled
        searchButton.isUserInteractionEnabled = isEnabled
        helpButton.isUserInteractionEnabled = isEnabled
        findButton.isUserInteractionEnabled = isEnabled
        findButton.setTitle(isEnabled ? viewModel.searchMeteoritesText : viewModel.searchingMeteoritesText, for: .normal)
    }

    private func showLocationRegion(location: CLLocation) {
        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        showLocationRegion(coordinate: center)
    }

    private func showLocationRegion(coordinate: CLLocationCoordinate2D) {
        let region = MKCoordinateRegion(center: coordinate, span: MKCoordinateSpan(latitudeDelta: 1, longitudeDelta: 1))
        mapView.setRegion(region, animated: true)
    }
}

extension MainViewController: ExpandableSearchButtonDelegate {

    func textEntered(text: String) {
        viewModel.updateLocation(by: text)
    }
}

extension MainViewController: MKMapViewDelegate {

    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        let point = CGPoint(x: targetImage.layer.position.x + (targetImage.bounds.width / 2 - 15), y: targetImage.layer.position.y)
        let margin = mapView.convert(point, toCoordinateFrom: mapView)
        viewModel.calculateRadius(center: mapView.centerCoordinate, margin: margin)
    }

    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let crater = overlay as? CraterCircleView
        let circleRenderer = MKCircleRenderer(overlay: overlay)
        circleRenderer.lineWidth = 6.0
        circleRenderer.fillColor = crater?.color.withAlphaComponent(0.25)
        circleRenderer.strokeColor = crater?.color
        return circleRenderer
    }

    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard annotation is MKPointAnnotation else { return nil }
        let customAnnotationView = self.getMeteoriteAnnotationView(in: mapView, for: annotation)
        customAnnotationView.meteoriteName = (annotation.title ?? "") ?? ""
        return customAnnotationView
    }

    private func getMeteoriteAnnotationView(in mapView: MKMapView, for annotation: MKAnnotation) -> MeteoriteAnnotationView {
        let identifier = "MeteoriteAnnotationView"
        if let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MeteoriteAnnotationView {
            annotationView.annotation = annotation
            return annotationView
        } else {
            let view = MeteoriteAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            view.canShowCallout = true
            return view
        }
    }
}
