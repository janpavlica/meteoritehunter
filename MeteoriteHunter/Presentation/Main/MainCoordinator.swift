//
//  MainCoordinator.swift
//  MeteoriteHunter
//
//  Created by Jan Pavlica on 20.01.2023.
//

import UIKit
import Jelly

typealias ResetResultsClosure = () -> Void

protocol MainCoordinatorFlow: Coordinator {

    func showMainView()
    func showResultsView(resetCompletion: @escaping ResetResultsClosure)
    func closeResultsView()
    func showAttemptResultView(with meteorites: [Meteorite])
    func closeAttemptResultView()
}

class MainCoordinator: NSObject, MainCoordinatorFlow {

    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController

    private var resetResultsCompletion: ResetResultsClosure?

    required init(_ navigationController: UINavigationController) {

        self.navigationController = navigationController
    }

    func start() {

        showMainView()
    }

    func showMainView() {

        let mainView = MainViewController()
        mainView.coordinator = self
        navigationController.setViewControllers([mainView], animated: false)
    }

    func showResultsView(resetCompletion: @escaping ResetResultsClosure) {

        self.resetResultsCompletion = resetCompletion

        let scoreView = ResultsViewController()
        scoreView.coordinator = self

        guard let lastView = navigationController.viewControllers.last else { return }
        lastView.present(scoreView, animated: true)
    }

    func closeResultsView() {

        resetResultsCompletion?()
        resetResultsCompletion = nil

        navigationController.dismiss(animated: true)
    }

    func showAttemptResultView(with meteorites: [Meteorite]) {

        let resultModel = AttemptResultViewModel(meterorites: meteorites)
        let resultView = AttemptResultViewController(viewModel: resultModel)

        guard let lastView = navigationController.viewControllers.last else { return }
        lastView.present(resultView, animated: true, completion: nil)
    }

    func closeAttemptResultView() {

        navigationController.dismiss(animated: true)
    }

    deinit {
        print("MainCoordinator deinit")
    }
}
