//
//  ExpandableSearchView.swift
//  MeteoriteHunter
//
//  Created by Jan Pavlica on 22.01.2023.
//

import UIKit

protocol ExpandableSearchButtonDelegate: AnyObject {

    func textEntered(text: String)
}

class ExpandableSearchButton: UIControl {

    weak var delegate: ExpandableSearchButtonDelegate?

    lazy private var searchText: UITextField = {
        let textField = UITextField()
        textField.delegate = self
        textField.backgroundColor = .clear
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: textField.frame.height))
        textField.leftViewMode = .always
        textField.font = UIFont.appFont(.regular, size: 18)
        return textField
    }()

    lazy private var searchButton: IconButton = {
        let button = IconButton()
        button.icon = UIImage(systemName: "magnifyingglass")
        button.configuration?.preferredSymbolConfigurationForImage = UIImage.SymbolConfiguration(weight: .bold)
        button.addTarget(self, action: #selector(self.search), for: .touchUpInside)
        return button
    }()

    var isExpanded: Bool = false {
        didSet {
            setupViews(animation: true)
        }
    }

    var placeholderText: String = "" {
        didSet {
            searchText.placeholder = placeholderText
        }
    }

    override var isEnabled: Bool {
        didSet {
            searchButton.isEnabled = isEnabled
        }
    }

    private var leadingSearchTextConstraint: NSLayoutConstraint?
    private var widthSearchTextConstraint: NSLayoutConstraint?

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func commonInit() {

        addSubViews()
        setupConstraints()
        setupViews(animation: false)
    }

    private func setupViews(animation: Bool) {

        searchText.text = ""
        searchText.layer.cornerRadius = searchButton.bounds.height / 2
        searchText.resignFirstResponder()
        self.searchButton.configuration?.baseForegroundColor = self.isExpanded ? .label : .white

        let duration = animation ? 0.25 : 0
        UIView.animate(withDuration: duration, animations: {
            self.searchText.removeConstraint(self.leadingSearchTextConstraint!)
            self.searchText.removeConstraint(self.widthSearchTextConstraint!)
            self.searchText.backgroundColor = self.isExpanded ? .systemBackground : .clear
            self.leadingSearchTextConstraint?.isActive = self.isExpanded
            self.widthSearchTextConstraint?.isActive = !self.isExpanded
            self.searchButton.configuration?.baseBackgroundColor = self.isExpanded ? .systemBackground : UIColor.appColor(AppColor.persianGreen)
            self.layoutIfNeeded()
        })
    }

    private func addSubViews() {

        let subviews = [searchText, searchButton]
        subviews.forEach {
            addSubview($0)
            $0.translatesAutoresizingMaskIntoConstraints = false
        }
    }

    private func setupConstraints() {

        searchButton.topAnchor.constraint(equalTo: topAnchor).isActive = true
        searchButton.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true

        self.layoutIfNeeded()

        searchText.topAnchor.constraint(equalTo: topAnchor).isActive = true
        searchText.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        searchText.heightAnchor.constraint(equalToConstant: searchButton.bounds.height).isActive = true

        leadingSearchTextConstraint = searchText.leadingAnchor.constraint(equalTo: leadingAnchor)
        leadingSearchTextConstraint?.isActive = isExpanded

        widthSearchTextConstraint = searchText.widthAnchor.constraint(equalToConstant: searchButton.bounds.width)
        widthSearchTextConstraint?.isActive = !isExpanded

        heightAnchor.constraint(equalToConstant: searchButton.bounds.height).isActive = true
    }

    @objc private func search() {

        guard let text = searchText.text else { return }
        if !text.isEmpty {
            let trimmed = text.trimmingCharacters(in: .whitespacesAndNewlines)
            delegate?.textEntered(text: trimmed)
        }
        searchText.text = ""
        searchText.resignFirstResponder()
        isExpanded.toggle()
    }
}

extension ExpandableSearchButton: UITextFieldDelegate {

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == searchText {
            search()
        }
        return true
    }
}
