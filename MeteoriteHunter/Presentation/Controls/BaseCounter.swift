//
//  BaseCounter.swift
//  MeteoriteHunter
//
//  Created by Jan Pavlica on 08.03.2023.
//

import UIKit

class BaseCounter: UIControl {

    lazy private var nameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.appFont(.medium, size: 18)
        label.textColor = UIColor.appColor(.grayText)
        label.numberOfLines = 1
        return label
    }()

    lazy private var valueLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.appFont(.black, size: 24)
        label.textColor = UIColor.appColor(.charcoalText)
        label.numberOfLines = 1
        return label
    }()

    var name: String = "" {
        didSet {
            nameLabel.text = name
        }
    }

    var value: String = "" {
        didSet {
            valueLabel.text = value
        }
    }

    var alignment: NSTextAlignment = .left {
        didSet {
            setupViews()
            setupConstraints()
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func commonInit() {

        addSubViews()
        setupConstraints()
        setupViews()
    }

    private func setupViews() {

        nameLabel.textAlignment = alignment
        valueLabel.textAlignment = alignment
    }

    private func addSubViews() {

        let subviews = [nameLabel, valueLabel]
        subviews.forEach {
            addSubview($0)
            $0.translatesAutoresizingMaskIntoConstraints = false
        }
    }

    private func setupConstraints() {

        nameLabel.topAnchor.constraint(equalTo: topAnchor).isActive = true

        switch alignment {
        case .right:
            nameLabel.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
            valueLabel.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        case .center:
            nameLabel.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
            valueLabel.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        default:
            nameLabel.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
            valueLabel.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        }

        valueLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 5).isActive = true
        valueLabel.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }
}
