//
//  IconButton.swift
//  MeteoriteHunter
//
//  Created by Jan Pavlica on 07.03.2023.
//

import UIKit

class IconButton: BaseButton {

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func commonInit() {
        configuration?.contentInsets = NSDirectionalEdgeInsets(top: 15, leading: 15, bottom: 15, trailing: 15)
    }

    var icon: UIImage? {
        didSet {
            configuration?.image = icon
        }
    }
}
