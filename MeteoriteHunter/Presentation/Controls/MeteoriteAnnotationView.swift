//
//  MeteoriteAnnotationView.swift
//  MeteoriteHunter
//
//  Created by Jan Pavlica on 23.01.2023.
//

import UIKit
import MapKit

class MeteoriteAnnotationView: MKAnnotationView {

    private let annotationFrame = CGRect(x: 0, y: 0, width: 100, height: 45)

    lazy private var nameLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 15, weight: .semibold)
        label.textColor = .label
        label.textAlignment = .center
        label.numberOfLines = .zero
        return label
    }()

    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    var meteoriteName: String = "" {
        didSet {
            self.nameLabel.text = meteoriteName
        }
    }

    private func commonInit() {

        self.frame = annotationFrame
        addSubViews()
        setupConstraints()
    }

    private func addSubViews() {

        let subviews = [nameLabel]
        subviews.forEach {
            addSubview($0)
            $0.translatesAutoresizingMaskIntoConstraints = false
        }
    }

    private func setupConstraints() {

        nameLabel.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        nameLabel.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        nameLabel.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
    }
}
