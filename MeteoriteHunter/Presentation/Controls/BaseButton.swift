//
//  BaseButton.swift
//  MeteoriteHunter
//
//  Created by Jan Pavlica on 07.03.2023.
//

import UIKit

class BaseButton: UIButton {

    private var defaultConfiguration: UIButton.Configuration = {
        var configuration = UIButton.Configuration.filled()
        configuration.cornerStyle = .capsule
        configuration.contentInsets = NSDirectionalEdgeInsets(top: 15, leading: 20, bottom: 15, trailing: 20)
        configuration.titleTextAttributesTransformer = UIConfigurationTextAttributesTransformer { incoming in
            var outgoing = incoming
            outgoing.font = UIFont.appFont(.semibold, size: 20)
            return outgoing
        }
        return configuration
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func commonInit() {
        configuration = defaultConfiguration
    }

    override func setTitle(_ title: String?, for state: UIControl.State) {
        super.setTitle(title?.uppercased(), for: state)
    }

    var baseBackgroundColor: UIColor? {
        didSet {
            configuration?.baseBackgroundColor = baseBackgroundColor
        }
    }
}
