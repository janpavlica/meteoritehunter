//
//  TitleLabel.swift
//  MeteoriteHunter
//
//  Created by Jan Pavlica on 07.03.2023.
//

import UIKit

class TitleLabel: UILabel {

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func commonInit() {
        font = UIFont.appFont(.black, size: 32)
        textColor = UIColor.appColor(AppColor.charcoalText)
        textAlignment = .center
        numberOfLines = .zero
    }

    override var text: String? {
        didSet {
            setParagraphStyle(alignment: .center, lineSpacing: 12)
        }
    }
}
