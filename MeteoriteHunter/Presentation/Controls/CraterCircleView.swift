//
//  CraterCircleView.swift
//  MeteoriteHunter
//
//  Created by Jan Pavlica on 26.01.2023.
//

import Foundation
import MapKit

class CraterCircleView: MKCircle {

    override init() {
        color = UIColor.appColor(AppColor.sandyBrown) ?? .gray
    }

    var color: UIColor
}
