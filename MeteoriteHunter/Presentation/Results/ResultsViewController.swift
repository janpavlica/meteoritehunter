//
//  ScoreViewController.swift
//  MeteoriteHunter
//
//  Created by Jan Pavlica on 20.01.2023.
//

import UIKit
import Combine

class ResultsViewController: UIViewController {

    var coordinator: MainCoordinatorFlow?

    private let viewModel: ResultsViewModel
    private var cancellable = Set<AnyCancellable>()

    lazy private var backgroundImage: UIImageView = {
        let view = UIImageView(image: UIImage(named: "planet-earth-clear"))
        view.layer.opacity = 0.1
        return view
    }()

    lazy private var titleLabel: TitleLabel = {
        let label = TitleLabel()
        return label
    }()

    lazy private var meteoritesTable: UITableView = {
        var table = UITableView()
        table.register(MeteoriteCell.self, forCellReuseIdentifier: MeteoriteCell.identifier)
        table.accessibilityIdentifier = "meteoritesTableView"
        table.delegate = self
        table.dataSource = self
        table.backgroundColor = .clear
        return table
    }()

    lazy private var resetButton: BaseButton = {
        let button = BaseButton()
        button.baseBackgroundColor = UIColor.appColor(AppColor.burntSienna)
        button.addTarget(self, action: #selector(reset(_:)), for: .touchUpInside)
        return button
    }()

    init(viewModel: ResultsViewModel = ResultsViewModel()) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {

        super.viewDidLoad()
        setupView()
        addSubViews()
        setupConstraints()
        setupBindings()
    }

    override func viewWillAppear(_ animated: Bool) {

        super.viewWillAppear(animated)
        viewModel.loadResults()
    }

    @objc private func reset(_ sender: UIButton?) {
        viewModel.resetResults()
        coordinator?.closeResultsView()
    }

    private func setupView() {

        titleLabel.text = viewModel.titleText
        resetButton.setTitle(viewModel.restartText, for: .normal)
        view.backgroundColor = .systemBackground
    }

    private func addSubViews() {

        let subviews = [backgroundImage, titleLabel, meteoritesTable, resetButton]
        subviews.forEach {
            self.view.addSubview($0)
            $0.translatesAutoresizingMaskIntoConstraints = false
        }
    }

    private func setupConstraints() {

        let safeGuide = view.safeAreaLayoutGuide

        titleLabel.topAnchor.constraint(equalTo: safeGuide.topAnchor, constant: 40).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: safeGuide.leadingAnchor, constant: 20).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: safeGuide.trailingAnchor, constant: -20).isActive = true
        titleLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true

        backgroundImage.widthAnchor.constraint(equalToConstant: view.bounds.height).isActive = true
        backgroundImage.heightAnchor.constraint(equalToConstant: view.bounds.height).isActive = true
        backgroundImage.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        backgroundImage.topAnchor.constraint(equalTo: safeGuide.centerYAnchor, constant: -100).isActive = true

        meteoritesTable.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 30).isActive = true
        meteoritesTable.leadingAnchor.constraint(equalTo: safeGuide.leadingAnchor, constant: 30).isActive = true
        meteoritesTable.trailingAnchor.constraint(equalTo: safeGuide.trailingAnchor, constant: -30).isActive = true
        meteoritesTable.bottomAnchor.constraint(equalTo: resetButton.topAnchor, constant: -35).isActive = true

        resetButton.leadingAnchor.constraint(equalTo: safeGuide.leadingAnchor, constant: 20).isActive = true
        resetButton.trailingAnchor.constraint(equalTo: safeGuide.trailingAnchor, constant: -20).isActive = true
        resetButton.bottomAnchor.constraint(equalTo: safeGuide.bottomAnchor, constant: -20).isActive = true
    }

    private func setupBindings() {

        viewModel.$meteorites
            .receive(on: RunLoop.main)
            .sink(receiveValue: { _ in
                self.meteoritesTable.reloadData()
            })
            .store(in: &cancellable)

        viewModel.$canReset
            .receive(on: RunLoop.main)
            .sink(receiveValue: { canReset in
                self.resetButton.isHidden = !canReset
            })
            .store(in: &cancellable)
    }
}

extension ResultsViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.meteorites.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        guard let cell = tableView.dequeueReusableCell(withIdentifier: MeteoriteCell.identifier, for: indexPath) as? MeteoriteCell else {
            return UITableViewCell()
        }
        let meteorite = viewModel.meteorites[indexPath.item]
        cell.viewModel = MeteoriteCellViewModel(meteorite: meteorite)
        return cell
    }
}

extension ResultsViewController: UITableViewDelegate {

}
