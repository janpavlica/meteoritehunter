//
//  SuccessViewController.swift
//  MeteoriteHunter
//
//  Created by Jan Pavlica on 26.01.2023.
//

import UIKit
import Combine
import Jelly

class AttemptResultViewController: UIViewController {

    var coordinator: MainCoordinatorFlow?

    private let viewModel: AttemptResultViewModel
    private var cancellable = Set<AnyCancellable>()
    private var animator: Animator?

    lazy private var titleLabel: TitleLabel = {
        let label = TitleLabel()
        return label
    }()

    lazy private var descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.appFont(.regular, size: 19)
        label.textColor = UIColor.appColor(AppColor.grayText)
        label.textAlignment = .center
        label.numberOfLines = .zero
        return label
    }()

    lazy private var resultImage: UIImageView = {
        let view = UIImageView()
        return view
    }()

    init(viewModel: AttemptResultViewModel) {

        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)

        let topCorners: CACornerMask = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        let uiConfiguration = PresentationUIConfiguration(cornerRadius: 35, backgroundStyle: .dimmed(alpha: 0.65), corners: topCorners)
        let size = PresentationSize(width: .fullscreen, height: .custom(value: 450))
        let alignment = PresentationAlignment(vertical: .bottom, horizontal: .center)
        let interaction = InteractionConfiguration(presentingViewController: self, completionThreshold: 0.5, dragMode: .canvas)
        let presentation = CoverPresentation(directionShow: .bottom, directionDismiss: .bottom, uiConfiguration: uiConfiguration,
                                             size: size, alignment: alignment, interactionConfiguration: interaction)

        self.animator = Animator(presentation: presentation)
        self.animator?.prepare(presentedViewController: self)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {

        super.viewDidLoad()
        setupView()
        addSubViews()
        setupConstraints()
        setupBindings()
    }

    private func setupView() {

        self.view.backgroundColor = .systemBackground
    }

    private func addSubViews() {

        let subviews = [titleLabel, resultImage, descriptionLabel]
        subviews.forEach {
            self.view.addSubview($0)
            $0.translatesAutoresizingMaskIntoConstraints = false
        }
    }

    private func setupConstraints() {

        let safeGuide = self.view.safeAreaLayoutGuide

        titleLabel.topAnchor.constraint(equalTo: safeGuide.topAnchor, constant: 40).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: safeGuide.leadingAnchor, constant: 20).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: safeGuide.trailingAnchor, constant: -20).isActive = true
        titleLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true

        resultImage.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 30).isActive = true
        resultImage.widthAnchor.constraint(equalToConstant: 200).isActive = true
        resultImage.heightAnchor.constraint(equalToConstant: 200).isActive = true
        resultImage.centerXAnchor.constraint(equalTo: safeGuide.centerXAnchor).isActive = true

        descriptionLabel.topAnchor.constraint(equalTo: resultImage.bottomAnchor, constant: 40).isActive = true
        descriptionLabel.leadingAnchor.constraint(equalTo: safeGuide.leadingAnchor, constant: 30).isActive = true
        descriptionLabel.trailingAnchor.constraint(equalTo: safeGuide.trailingAnchor, constant: -30).isActive = true
    }

    private func setupBindings() {

        viewModel.$numberOfMeteorites
            .receive(on: RunLoop.main)
            .sink(receiveValue: { number in

                self.titleLabel.text = number > 0 ? self.viewModel.successTitleText : self.viewModel.failureTitleText

                switch number {
                case 0:
                    self.resultImage.image = UIImage(named: "planet-earth-clear")
                case 1:
                    self.resultImage.image = UIImage(named: "planet-earth")
                default:
                    self.resultImage.image = UIImage(named: "planet-earth-big")
                }
            })
            .store(in: &cancellable)

        viewModel.$descriptionResult
            .receive(on: RunLoop.main)
            .sink(receiveValue: { description in
                self.descriptionLabel.text = description
            })
            .store(in: &cancellable)
    }
}
