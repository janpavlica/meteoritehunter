//
//  MeteoriteCell.swift
//  MeteoriteHunter
//
//  Created by Jan Pavlica on 29.01.2023.
//

import UIKit
import Combine

class MeteoriteCell: UITableViewCell {

    static let identifier = "MeteoriteCell"

    var viewModel: MeteoriteCellViewModel! {
        didSet {
            setupViewModel()
        }
    }

    lazy private var nameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.appFont(.semibold, size: 20)
        label.textColor = UIColor.appColor(AppColor.charcoalText)
        return label
    }()

    lazy private var yearLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.appFont(.regular, size: 18)
        label.textColor = UIColor.appColor(.grayText)
        return label
    }()

    lazy private var distanceLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.appFont(.semibold, size: 20)
        label.textColor = UIColor.appColor(AppColor.charcoalText)
        return label
    }()

    lazy private var massLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.appFont(.regular, size: 18)
        label.textColor = UIColor.appColor(.grayText)
        return label
    }()

    var cancellable = Set<AnyCancellable>()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {

        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
        addSubViews()
        setupConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {

        super.prepareForReuse()
        cancellable.removeAll()
    }

    private func setupView() {

        self.backgroundColor = .clear
        self.selectionStyle = .none
    }

    private func addSubViews() {

        let subviews = [nameLabel, distanceLabel, yearLabel, massLabel]
        subviews.forEach {
            contentView.addSubview($0)
            $0.translatesAutoresizingMaskIntoConstraints = false
        }
    }

    private func setupConstraints() {

        nameLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10).isActive = true
        nameLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true

        distanceLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10).isActive = true
        distanceLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true

        yearLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 2).isActive = true
        yearLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
        yearLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10).isActive = true

        massLabel.topAnchor.constraint(equalTo: distanceLabel.bottomAnchor, constant: 2).isActive = true
        massLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
        massLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10).isActive = true
    }

    private func setupViewModel() {

        nameLabel.text = viewModel.name
        yearLabel.text = viewModel.year
        distanceLabel.text = viewModel.distance
        massLabel.text = viewModel.mass
    }
}
