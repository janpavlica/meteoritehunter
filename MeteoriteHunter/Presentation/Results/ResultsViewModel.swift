//
//  ScoreViewModel.swift
//  MeteoriteHunter
//
//  Created by Jan Pavlica on 23.01.2023.
//

import Foundation
import Combine

enum ResultsViewModelState: Equatable {
    case loading
    case empty
    case finished
    case error(NetworkError)
}

final class ResultsViewModel {

    let titleText = NSLocalizedString("scoreTitle", comment: "")
    let restartText = NSLocalizedString("scoreStartAgain", comment: "")

    @Published private(set) var meteorites: [Meteorite] = []
    @Published private(set) var canReset: Bool = false
    @Published private(set) var state: ResultsViewModelState = .loading

    @Inject private var useCase: ResultsUseCase

    private var cancellable: AnyCancellable?

    func loadResults() {

        cancellable = loadMeteorites()
            .receive(on: DispatchQueue.main)
            .sink { completion in
                switch completion {
                case .failure(let error):
                    self.state = .error(error as! NetworkError)
                    print(error)
                default: break
                }
            } receiveValue: { meteorites in
                self.meteorites = meteorites
                if meteorites.isEmpty {
                    self.state = .empty
                } else {
                    self.state = .finished
                }
            }

        canReset = useCase.existsAnyShot()
    }

    func resetResults() {

        do {
            try useCase.resetResults()
        } catch {
            print(error)
        }
    }
}

extension ResultsViewModel {

    private func loadMeteorites() -> Future<[Meteorite], Error> {

        return Future({ [self] promise in
            Task {
                do {
                    let meteorites = try useCase.getMeteorites()
                    promise(.success(meteorites))
                } catch {
                    promise(.failure(error))
                }
            }
        })
    }
}
