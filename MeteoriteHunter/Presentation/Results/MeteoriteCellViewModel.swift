//
//  MeteoriteCellViewModel.swift
//  MeteoriteHunter
//
//  Created by Jan Pavlica on 29.01.2023.
//

import Foundation

final class MeteoriteCellViewModel {

    var name: String {
        meteorite.name
    }

    var year: String {
        return dateFormatter.string(from: meteorite.landingDate)
    }

    var mass: String {
        massFormatter.string(fromValue: Double(meteorite.mass), unit: .gram)
    }

    var distance: String {
        guard let distance = meteorite.distance else { return "" }
        guard let distanceString = numberFormatter.string(from: (distance / 1000) as NSNumber) else { return "" }
        return distanceString
    }

    private var meteorite: Meteorite

    private var numberFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.locale = Locale.current
        formatter.numberStyle = .decimal
        formatter.maximumFractionDigits = 2
        formatter.positiveSuffix = " km"
        return formatter
    }()

    private var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.locale = Locale.current
        formatter.dateFormat = "yyyy"
        return formatter
    }()

    private var massFormatter: MassFormatter = {
        let formatter = MassFormatter()
        formatter.numberFormatter.locale = Locale.current
        return formatter
    }()

    init(meteorite: Meteorite) {
        self.meteorite = meteorite
    }
}
