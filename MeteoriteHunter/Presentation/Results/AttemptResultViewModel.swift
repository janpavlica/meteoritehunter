//
//  SuccessViewModel.swift
//  MeteoriteHunter
//
//  Created by Jan Pavlica on 26.01.2023.
//

import Foundation
import Combine

final class AttemptResultViewModel {

    let successTitleText = NSLocalizedString("resultSuccessTitle", comment: "")
    let failureTitleText = NSLocalizedString("resultFailureTitle", comment: "")
    let successText = NSLocalizedString("resultSuccessText", comment: "")
    let moreSuccessText = NSLocalizedString("resultMoreSuccessText", comment: "")
    let failureText = NSLocalizedString("resultFailureText", comment: "")

    @Published private(set) var numberOfMeteorites: Int = 0
    @Published private(set) var descriptionResult: String = ""

    private var meteorites: [Meteorite]
    private var numberFormatter: NumberFormatter

    init(meterorites: [Meteorite], numberFormatter: NumberFormatter = NumberFormatter()) {

        self.meteorites = meterorites
        self.numberFormatter = numberFormatter

        numberOfMeteorites = meterorites.count

        switch numberOfMeteorites {
        case 0:
            descriptionResult = failureText
        case 1:
            guard let meteorite = meterorites.first else { return }
            descriptionResult = String(format: successText, meteorite.name)
        default:
            guard let meteoritesString = numberFormatter.string(from: meterorites.count as NSNumber) else { return }
            let meteoritesSuffix = StringUtils.meteoritesSuffix(meteorites: meterorites.count)
            guard let biggestMeteorite = (meterorites.max { $0.mass < $1.mass }) else { return }
            descriptionResult = String(format: moreSuccessText, meteoritesString, meteoritesSuffix, biggestMeteorite.name)
        }
    }
}
